using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Gateway
{
    public class RoutingMiddleware
    {
        private static readonly int DEFAULT_BUFFER_SIZE = 81920;

        private readonly HttpClient _client;
        private readonly RoutingSetting _setting;
        private readonly RequestDelegate _next;

        public RoutingMiddleware(RequestDelegate next, RoutingSetting setting, IHttpClientFactory clientFactory)
        {
            _next = next;
            _setting = setting;
            _client = clientFactory.CreateClient();
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var uri = GetRoutingUri(context.Request);

            if (uri != null)
                await RouteRequest(context, uri);
            else
                await _next(context);
        }

        private async Task RouteRequest(HttpContext context, Uri uri)
        {
            using (var request = PrepareRequest(context, uri))
            {
                if (IsBodyRequest(context.Request))
                    request.Content = new StreamContent(context.Request.Body);

                using (var response = await _client.SendAsync(request))
                {
                    context.Response.StatusCode = (int)response.StatusCode;

                    using (var stream = await response.Content.ReadAsStreamAsync())
                    {
                        foreach (var header in response.Headers)
                            context.Response.Headers.Add(header.Key, header.Value.ToArray());

                        await stream.CopyToAsync(context.Response.Body, DEFAULT_BUFFER_SIZE, context.RequestAborted);
                    }
                }
            }
        }

        private HttpRequestMessage PrepareRequest(HttpContext context, Uri uri)
        {
            var request = new HttpRequestMessage();

            if (IsBodyRequest(context.Request))
                request.Content = new StreamContent(context.Request.Body);

            request.Headers.Add("X-Forwarded-For", context.Connection.RemoteIpAddress.ToString());
            request.Headers.Add("X-Forwarded-Host", context.Request.Host.ToString());
            request.Headers.Add("X-Forwarded-Proto", context.Request.Scheme);
            request.Headers.Host = uri.Host;
            request.RequestUri = uri;

            foreach (var header in context.Request.Headers)
            {
                if (request.Headers.Contains(header.Key))
                    continue;

                request.Headers.Add(header.Key, header.Value.ToArray());
            }

            return request;
        }

        private bool IsBodyRequest(HttpRequest request)
        {
            return !HttpMethods.IsGet(request.Method)
                && !HttpMethods.IsHead(request.Method)
                && !HttpMethods.IsTrace(request.Method)
                && !HttpMethods.IsOptions(request.Method);
        }

        private Uri GetRoutingUri(HttpRequest request)
        {
            foreach (var route in _setting.Routes)
            {
                if (route.Value.Path == request.Path)
                    return new Uri(route.Value.Target);
            }

            return null;
        }
    }
}