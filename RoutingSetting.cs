using System;
using System.Collections.Generic;

namespace Gateway
{
    public class RoutingSetting
    {
        public Dictionary<string, Route> Routes { get; set; }

        public class Route
        {
            public string Path { get; set; }

            public string Target { get; set; }
        }
    }
}